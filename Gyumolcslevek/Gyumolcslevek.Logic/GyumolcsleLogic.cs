﻿using Gyumolcslevek.Data;
using Gyumolcslevek.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyumolcslevek.Logic
{
    public class GyumolcsleLogic : IGyumolcsleLogic
    {
        IGyumolcsleRepisory gyumolcsrepo;
        public GyumolcsleLogic(IGyumolcsleRepisory gyumolcsrepo)
        {
            this.gyumolcsrepo = gyumolcsrepo;
        }

        public void AddGyumolcsle(string newBrand, string newCountry, string newFruits, int newPrice, int newAmount, int newProfit)
        {
            Gyumolcsle gyumolcsle = new Gyumolcsle();
            gyumolcsle.Brand = newBrand;
            gyumolcsle.Country = newCountry;
            gyumolcsle.Fruits = newFruits;
            gyumolcsle.Prize = newPrice;
            gyumolcsle.Amount = newAmount;
            gyumolcsle.Profit = newProfit;
            gyumolcsrepo.Insert(gyumolcsle);
        }

        public Gyumolcsle GetOneGyumolcsle(int id)
        {
            return gyumolcsrepo.GetOne(id);
        }

        public IList<Gyumolcsle> GetAllGyumolcsle()
        {
            return gyumolcsrepo.GetAll().ToList();
        }

        public bool ChangeGyumolcsle(int Id, string newBrand, string newCountry, string newFruits, int newPrice, int newAmount, int newProfit)
        {
            var brand = gyumolcsrepo.GetAll().SingleOrDefault(x => x.Brand == newBrand);
            return gyumolcsrepo.ChangeGyumolcsle(Id, newBrand, newCountry, newFruits, newPrice, newAmount, newProfit);
        }

        public bool RemoveGyumolcsle(int id)
        {
            return gyumolcsrepo.Remove(id);
        }
    }
}
