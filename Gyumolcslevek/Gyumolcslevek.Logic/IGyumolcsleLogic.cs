﻿using Gyumolcslevek.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyumolcslevek.Logic
{
    public interface IGyumolcsleLogic // Avoid: GOD OBJECT, too many responsibilities!
    {
        void AddGyumolcsle(string newBrand, string newCountry, string newFruits, int newPrice, int newAmount, int newProfit);
        Gyumolcsle GetOneGyumolcsle(int id);
        IList<Gyumolcsle> GetAllGyumolcsle();
        bool ChangeGyumolcsle(int Id, string newBrand, string newCountry, string newFruits, int newPrice, int newAmount, int newProfit);
        bool RemoveGyumolcsle(int id);
    }
}
