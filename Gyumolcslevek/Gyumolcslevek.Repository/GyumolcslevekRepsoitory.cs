﻿using CarShop.Repository;
using Gyumolcslevek.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyumolcslevek.Repository
{
    public class GyumolcslevekRepsoitory : Repository<Gyumolcsle>, IGyumolcsleRepisory

    {
        public GyumolcslevekRepsoitory(DbContext ctx) : base(ctx) { }

        public void ChangePrice(int id, int newprice)
        {
            var gyumolcsle = GetOne(id);
            gyumolcsle.Prize = newprice;
            ctx.SaveChanges();
        }

        public override Gyumolcsle GetOne(int id)
        {
            return GetAll().SingleOrDefault(x => x.Id == id);
        }

        public bool ChangeGyumolcsle(int id, string newBrand, string newCountry, string newFruits, int newPrice, int newAmount, int newProfit)
        {
            Gyumolcsle entity = GetOne(id);
            if (entity == null) return false;
            entity.Brand = newBrand;
            entity.Country = newCountry;
            entity.Fruits = newFruits;
            entity.Prize = newPrice;
            entity.Amount = newAmount;
            entity.Profit = newProfit;
            ctx.SaveChanges();
            return true;
        }
    }
}
