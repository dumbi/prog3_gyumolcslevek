﻿using Gyumolcslevek.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyumolcslevek.Repository
{
    public interface IRepository<T> where T : class
    {
        T GetOne(int id);
        IQueryable<T> GetAll();

        bool Remove(int id);
        void Insert(T entity);
    }
    public interface IGyumolcsleRepisory : IRepository<Gyumolcsle>
    {
        bool ChangeGyumolcsle(int id, string newBrand, string newCountry, string newFruits, int newPrice, int newAmount, int newProfit);
    }
}
