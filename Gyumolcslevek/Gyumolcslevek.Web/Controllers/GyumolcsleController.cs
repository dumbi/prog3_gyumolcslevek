﻿using AutoMapper;
using Gyumolcslevek.Logic;
using Gyumolcslevek.Web.Models;
using Gyumolcslevek.Repository;
using Gyumolcslevek.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Gyumolcslevek.Web.Controllers
{
    public class GyumolcsleController : Controller
    {

        IGyumolcsleLogic logic;
        IMapper mapper;
        GyumolcsleViewModel vm;

        public GyumolcsleController()
        {
            GyumolcslevekEntities1 ctx = new GyumolcslevekEntities1();
            GyumolcslevekRepsoitory gymolcsrepo = new GyumolcslevekRepsoitory(ctx);
            logic = new GyumolcsleLogic(gymolcsrepo);
            mapper = MapperFactory.CreateMApper();
            vm = new GyumolcsleViewModel();
            vm.EditedGyumolcsle = new GyumolcsleModel();
            vm.ListOfGyumolcsles = new List<GyumolcsleModel>();
            var cars = logic.GetAllGyumolcsle();
            foreach (var item in cars)
            {
                vm.ListOfGyumolcsles.Add(GetGyumolcsleModel(item.Id));
            }
        }

        private GyumolcsleModel GetGyumolcsleModel(int id)
        {
            Gyumolcsle gyumolcsle = logic.GetOneGyumolcsle(id);
            return mapper.Map<Data.Gyumolcsle, Models.GyumolcsleModel>(gyumolcsle);
        }
        // GET: Gyumolcsle
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("GyumolcsleIndex", vm);
        }

        // GET: Gyumolcsle/Details/5
        public ActionResult Details(int id)
        {
            return View("GyumolcsleDetails", GetGyumolcsleModel(id));
        }

        // GET: Gyumolcsle/Create/5
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.RemoveGyumolcsle(id))
            {
                TempData["editResult"] = "Delete OK";
            }
            return RedirectToAction("Index");
        }

        // GET: Gyumolcsle/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedGyumolcsle = GetGyumolcsleModel(id);

            return View("GyumolcsleIndex", vm);
        }

        //POST /Gyumolcsle/Edit + Car + editAction
        [HttpPost]
        public ActionResult Edit(GyumolcsleModel gyumolcsle, string editAction)
        {
            if (ModelState.IsValid && gyumolcsle != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.AddGyumolcsle(
                        gyumolcsle.BrandName,
                        gyumolcsle.Country,
                        gyumolcsle.Fruits,
                        gyumolcsle.Price,
                        gyumolcsle.Amount,
                        gyumolcsle.Profit);
                }
                else
                {
                    bool success = logic.ChangeGyumolcsle(
                        gyumolcsle.Id,
                        gyumolcsle.BrandName,
                        gyumolcsle.Country,
                        gyumolcsle.Fruits,
                        gyumolcsle.Price,
                        gyumolcsle.Amount,
                        gyumolcsle.Profit);
                    if (!success) TempData["editResult"] = "Edit FAIL";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedGyumolcsle = gyumolcsle;

                return View("GyumolcsleIndex", vm);
            }
        }
    }
}
