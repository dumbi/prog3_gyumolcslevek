﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gyumolcslevek.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMApper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Gyumolcslevek.Data.Gyumolcsle, Gyumolcslevek.Web.Models.GyumolcsleModel>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                ForMember(dest => dest.BrandName, map => map.MapFrom(src => src.Brand)).
                ForMember(dest => dest.Price, map => map.MapFrom(src => src.Prize)).
                ForMember(dest => dest.Country, map => map.MapFrom(src => src.Country)).
                ForMember(dest => dest.Fruits, map => map.MapFrom(src => src.Fruits)).
                ForMember(dest => dest.Amount, map => map.MapFrom(src => src.Amount)).
                ForMember(dest => dest.Profit, map => map.MapFrom(src => src.Profit));
            });
            return config.CreateMapper();
        }
    }
}