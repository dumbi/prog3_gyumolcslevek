﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Gyumolcslevek.Web.Models
{
    public class GyumolcsleModel
    {
        [Display(Name = "Gyumolcsle Id")]
        [Required]
        public int Id { get; set; }
        [Display(Name = "Brand Name")]
        [Required]
        public string BrandName { get; set; }
        [Display(Name = "Country")]
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string Country { get; set; }
        [Display(Name = "Fruits")]
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string Fruits { get; set; }
        [Display(Name = "Amount")]
        [Required]
        public int Amount { get; set; }

        [Display(Name = "Price(per hekto)")]
        [Required]
        public int Price { get; set; }

        [Display(Name = "Profit")]
        [Required]
        public int Profit { get; set; }
    }
}