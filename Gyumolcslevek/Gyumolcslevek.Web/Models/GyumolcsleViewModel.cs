﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gyumolcslevek.Web.Models
{
    public class GyumolcsleViewModel
    {
        public GyumolcsleModel EditedGyumolcsle {get; set;}
        public IList<GyumolcsleModel> ListOfGyumolcsles {get; set;}
    }
}